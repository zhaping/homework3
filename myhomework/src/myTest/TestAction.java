package myTest;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import structs.Action;
import structs.form.ActionForm;

public class TestAction implements Action {

	public String excute(HttpServletRequest request, ActionForm actionForm,
			Map<String, String> map) {
		String fromUrl="";
		MyTestForm form=(MyTestForm) actionForm;
		String url="";
		if(form.getPassword().equals("123")&&form.getUsername().equals("hp")){
			url="success";
			fromUrl=map.get(url);
		}else if(form.getPassword().equals("321")&&form.getUsername().equals("hp")){
			url="failed";
			fromUrl=map.get(url);
		}
		System.out.println(map.get(url));
		return fromUrl;
	}

}
