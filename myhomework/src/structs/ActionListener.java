package structs;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ActionListener implements ServletContextListener {

	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("info:注销");

	}

	public void contextInitialized(ServletContextEvent arg0) {
		ServletContext context=arg0.getServletContext();
		String tomcat=context.getRealPath("\\");
		String filePath=context.getInitParameter("structs.xml");
		try {
			Map<String,XMLBean> map=Structs_xml.structs_xml(tomcat+filePath);
			context.setAttribute("structs", map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("info:ok");

	}

}
