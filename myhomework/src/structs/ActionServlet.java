package structs;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import spring.annotation.Controller;
import structs.form.ActionForm;
import structs.form.AnnotationForm;
import structs.form.FullForm;
import structs.form.HttpServletForm;

public class ActionServlet extends HttpServletForm  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			doPost(request, response);
	}
	
	@Controller(value="/common/hello")
	public String output(){
		String classPath= Thread.currentThread() .getStackTrace()[1].getClassName();
		String method = Thread.currentThread() .getStackTrace()[1].getMethodName();
		System.out.println(classPath+"====="+method);
		String ss=AnnotationForm.analysis(classPath, method);
		return ss;
	}
	
	@Controller(value="/common/hello")
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String path=this.getPath(request.getServletPath());
		Map<String,XMLBean> map=(Map<String, XMLBean>) this.getServletContext().getAttribute("structs");
		XMLBean xml=map.get(path);
		String formClass=xml.getFormClass();//鍝釜绫�
		ActionForm form=FullForm.fullForm(formClass, request);
		request.setAttribute("form", form);
		String actionType=xml.getActionType();
		String url="";
		Action action=null;
		try{
			Class clazz=Class.forName(actionType);
			action=(Action) clazz.newInstance();
			url=action.excute(request, form, xml.getMapForword());
			String toUrl=output();
			if("".equals(url)){
				url=toUrl+".jsp";
			}
			//String ss=AnnotationForm.analysis(classPath, method);
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		RequestDispatcher dis=request.getRequestDispatcher(url);
		dis.forward(request, response);
	}
	public String getPath(String path){
		return path.split("\\.")[0];
	}
}
