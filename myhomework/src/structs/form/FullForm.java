package structs.form;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

public class FullForm {
	public FullForm() {
		// TODO Auto-generated constructor stub
	}
	
	public static ActionForm fullForm(String classPath,HttpServletRequest request){
		ActionForm action=null;
		try{
			Class clazz=Class.forName(classPath);
			action=(ActionForm) clazz.newInstance();
			Field[] fields=clazz.getDeclaredFields();
			for(Field f:fields){
				f.setAccessible(true);
				f.set(action, request.getParameter(f.getName()));
				f.setAccessible(false);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return action;
	}
}
