package structs;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

public class Structs_xml {
	
	
	public static HashMap<String, XMLBean> structs_xml(String classPath) throws Exception{
		HashMap<String, XMLBean> xmlBeans=new HashMap<String, XMLBean>();
		SAXBuilder builder=new SAXBuilder();
		Document document=builder.build(new File(classPath));
		Element root=document.getRootElement();
		
		Element actionroot=root.getChild("action-mapping");
		List<Element> action=actionroot.getChildren();
		String path="";
		
		
		for(Element ele:action){
			XMLBean xml=new XMLBean();
			String name=ele.getAttributeValue("name");
			xml.setBeanName(name);
			Element actionForm= root.getChild("formbeans");
			List<Element> form=actionForm.getChildren();
			
			
			for(Element f:form){
				if(name.equals(f.getAttributeValue("name"))){
					String formClass=f.getAttributeValue("class");
					xml.setFormClass(formClass);
					break;
				}
			}
			path=ele.getAttributeValue("path");
			xml.setPath(path);
			String type=ele.getAttributeValue("type");
			xml.setActionType(type);
			
			List<Element> forwordList=ele.getChildren();
			Map<String,String> map=new HashMap<String, String>();
			for(Element e:forwordList){
				String fname=e.getAttributeValue("name");
				String fvalue=e.getAttributeValue("value");
				map.put(fname, fvalue);
			}
			xml.setMapForword(map);
			xmlBeans.put(path, xml);
		}
		return xmlBeans;
	}
	
}
