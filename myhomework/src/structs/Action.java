package structs;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import structs.form.ActionForm;

public interface Action {
	String excute(HttpServletRequest request,ActionForm actionForm,Map<String,String> map);
}
