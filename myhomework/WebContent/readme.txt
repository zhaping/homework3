
1、action 要继承Action接口  bean 要继承actionForm
2、父节点  structs  
	子节点 formbeans  bean类配置 
	子节点action-mapping action类的配置
3、formbeans 
	可以有多个bean name属性要和下面的action的name对应 
	class 为bean的全类名
4、action-mapping  action类型配置  可配置多个action
	type对应的action的全类名  path对应访问的action路径
	子类名forward 可配置多个name在后台进行逻辑判断，value对应的页面

测试 跳向成功页面  姓名：hp 密码 123 ,
测试跳向失败页面  
系统会自动储存信息  在页面可强转输出

注解 
当找不到页面时，跳到注解的页面
